<?php
namespace Ikx\NS\Model;

class Verstoring {
    public $id = '';
    public $baanvakBeperking = [];
    public $reden = '';
    public $extraReistijd = '';
    public $leafletUrl = '';
    public $reisadviezen;
    public $geldigheidsLijst = [];
    public $verwachting = '';
    public $gevolg = '';
    public $gevolgType = '';
    public $fase = '';
    public $impact = 0;
    public $maatschappij = 0;
    public $alternatiefVervoer = '';
    public $landelijk = false;
    public $oorzaak = '';
    public $header = '';
    public $meldtijd = '';
    public $periode = '';
    public $type = '';
    public $baanvakken = [];
    public $trajecten = [];
    public $versie = '';
    public $volgnummer = '';
    public $prioriteit = 0;
}