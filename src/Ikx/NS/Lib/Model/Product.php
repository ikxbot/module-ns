<?php
namespace Ikx\NS\Model;

class Product {
    public $number = '';
    public $categoryCode = '';
    public $shortCategoryName = '';
    public $longCategoryName = '';
    public $operatorCode = '';
    public $operatorName = '';
    public $type = '';
    public $displayName = '';
}