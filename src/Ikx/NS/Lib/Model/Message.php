<?php
namespace Ikx\NS\Model;

class Message {
    public $externalId = '';
    public $head = '';
    public $text = '';
    public $lead = '';
    public $type = '';
    public $startDate = '';
    public $endDate = '';
    public $startTime = '';
    public $endTime = '';
}