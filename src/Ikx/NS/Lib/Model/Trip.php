<?php
namespace Ikx\NS\Model;

class Trip {
    public $plannedDurationInMinutes = 0;
    public $transfers = 0;
    public $status = '';
    public $legs = [];
    public $overviewPolyLine = [];
    public $checksum = '';
    public $crowdForecast = '';
    public $punctuality = 0.00;
    public $ctxRecon = '';
    public $actualDurationInMinutes = 0;
    public $idx = 0;
    public $optimal = false;
    public $fares = [];
    public $productFare;
    public $shareUrl = [];
    public $type = '';
    public $realtime = false;
    public $travelAssistanceInfo;
    public $firstLeg;
    public $lastLeg;
}