<?php
namespace Ikx\NS\Model;

class Stop {
    public $name = '';
    public $lat = 0.00;
    public $lng = 0.00;
    public $city = '';
    public $countryCode = '';
    public $uicCode = '';
    public $weight = 0;
    public $products = 0;
    public $routeIdx = 0;
    public $plannedDepartureDateTime = '';
    public $plannedDepartureTimeZoneOffset = 0;
    public $actualDepartureDateTime = '';
    public $actualDepartureTimeZoneOffset = 0;
    public $plannedDepartureTrack = '';
    public $actualDepartureTrack = '';
    public $plannedArrivalDateTime = '';
    public $plannedArrivalTimeZoneOffset = 0;
    public $actualArrivalDateTime = '';
    public $actualArrivalTimeZoneOffset = 0;
    public $plannedArrivalTrack = '';
    public $actualArrivalTrack = '';
    public $departureDelayInSeconds = 0;
    public $arrivalDelayInSeconds = 0;
    public $cancelled = false;
    public $passing = false;
    public $quayCode = '';
}