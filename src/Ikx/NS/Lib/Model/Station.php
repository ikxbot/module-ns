<?php

namespace Ikx\NS\Model;

class Station
{
    public $sporen = [];
    public $synoniemen = [];
    public $heeftFaciliteiten = false;
    public $heeftVertrektijden = false;
    public $heeftReisassistentie = false;
    public $code = '';
    public $namen;
    public $stationType = '';
    public $land = '';
    public $lat = 0.00;
    public $lng = 0.00;
    public $radius = 0;
    public $naderenRadius = 0;
    public $uiccode = '';
    public $evacode = '';
}