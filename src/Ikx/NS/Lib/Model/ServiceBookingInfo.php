<?php

namespace Ikx\NS\Model;

class ServiceBookingInfo
{
    public $name = '';
    public $tripLegIndex = '';
    public $stationUic = '';
    public $serviceTypeIds = [];
    public $defaultAssistanceValue = false;
    public $canChangeAssistance = '';
    public $message = '';
}