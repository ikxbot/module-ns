<?php

namespace Ikx\NS\Model;

class Note
{
    public $value = '';
    public $key = '';
    public $noteType = '';
    public $priority = 0;
    public $routeIdxFrom = 0;
    public $routeIdxTo = 0;
    public $link = [];
    public $alternativeTransport = false;
    public $presentationRequired = false;
}