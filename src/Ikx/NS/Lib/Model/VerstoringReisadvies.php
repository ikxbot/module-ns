<?php
namespace Ikx\NS\Model;

class VerstoringReisadvies {
    /**
     * @var string
     */
    public $titel = '';

    /**
     * @var array
     */
    public $advies = [];
}