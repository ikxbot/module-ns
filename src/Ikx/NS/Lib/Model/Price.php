<?php

namespace Ikx\NS\Model;

class Price
{
    public $totalPriceInCents = 0;
    public $priceDifferenceInCentsBetweenFirstAndSecondClass = 0;
    public $routeDesignation = '';
    public $alternativeRouteDesignations = [];
    public $operatorName = '';
    public $travelDiscount = '';
    public $travelClass = '';
    public $reisrecht;
    public $travelProducts = [];
}