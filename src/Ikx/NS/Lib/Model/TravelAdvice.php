<?php
namespace Ikx\NS\Model;

class TravelAdvice {
    public $trips = [];
    public $scrollRequestBackwardContext = '';
    public $scrollRequestForwardContext = '';
    public $message = '';
    public $firstDeparture = '';
    public $firstArrival = '';
    public $firstTrip;
    public $lastTripDeparture = '';
    public $lastTripArrival = '';
    public $lastTrip;
}