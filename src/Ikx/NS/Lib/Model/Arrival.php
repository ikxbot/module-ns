<?php
namespace Ikx\NS\Model;

class Arrival {
    public $origin = '';
    public $name = '';
    public $plannedTrack = '';
    public $actualTrack = '';
    public $product;
    public $trainCategory = '';
    public $cancelled = false;
    public $journeyDetailRef = '';
    public $plannedDateTime = '';
    public $actualDateTime = '';
    public $messages = [];
    public $routeStations = [];
    public $actualOrPlannedTime = '';
}