<?php
namespace Ikx\NS\Lib\NS;

use Ikx\NS\Lib\Exception\StationNotFoundException;

class Api {
    /**
     * @var string NS API key
     */
    private $apikey = '';

    private $stations = [];

    public function __construct(string $apikey) {
        $this->apikey = $apikey;
    }

    public function departure($station = '', $maxJourneys = 5) {
        if (!$this->stations) {
            $this->fetchStations();
        }

        $station = $this->findStation($station);
        $departures = $this->request('v2', 'departures', [
            'station'           => $station,
            'maxJourneys'       => $maxJourneys
        ]);

        return ['station' => $this->stations[$station]['lang'] . ' (' . $this->stations[$station]['code'] . ')', 'data' => $departures->payload->departures];
    }

    public function searchStation($station = '') {
        $options = [];

        foreach($this->stations as $stationData) {
            similar_text(strtoupper($station), strtoupper($stationData['code']), $p1);
            similar_text(strtoupper($station), strtoupper($stationData['lang']), $p2);
            similar_text(strtoupper($station), strtoupper($stationData['middel']), $p3);
            similar_text(strtoupper($station), strtoupper($stationData['kort']), $p4);

            if ($p1 > 60 || $p2 > 60 || $p3 > 60 || $p4 > 60) {
                $options[] = $stationData['lang'];
            }
        }

        return $options;
    }

    private function findStation($station = '') {
        foreach($this->stations as $stationData) {
            if (
                strtoupper($stationData['code']) == strtoupper($station) ||
                strtoupper($stationData['lang']) == strtoupper($station) ||
                strtoupper($stationData['middel']) == strtoupper($station) ||
                strtoupper($stationData['kort']) == strtoupper($station)
            ) {
                return $stationData['code'];
            }
        }

        throw new StationNotFoundException();
    }

    private function fetchStations() {
        $stations = $this->request('v2', 'stations');
        foreach($stations->payload as $station) {
            $this->stations[$station->code] = [
                'lang'      => $station->namen->lang,
                'kort'      => $station->namen->kort,
                'middel'    => $station->namen->middel,
                'code'      => $station->code
            ];
        }
    }

    private function request($version = 'v3', $endpoint = '', $data = []) {
        $baseUrl = "https://ns-api.nl/reisinfo/api";
        $url = sprintf('%s/%s/%s', $baseUrl, $version, $endpoint);

        if ($data != []) {
            $url .= '?' . http_build_query($data);
        }

        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_RETURNTRANSFER  => true,
            CURLOPT_URL             => $url,
            CURLOPT_HTTPHEADER      => [
                sprintf('X-Api-Key: %s', $this->apikey)
            ]
        ]);

        $buffer = curl_exec($curl);
        curl_close($curl);

        echo $buffer;

        return json_decode($buffer);
    }
}