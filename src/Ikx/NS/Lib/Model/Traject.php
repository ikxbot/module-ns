<?php

namespace Ikx\NS\Model;

class Traject
{
    public $vervoerders = 0;
    public $naamVervoer = '';
    public $cdTariefpuntVan = 0;
    public $cdTariefpuntNaar = 0;
    public $afstand1eKlasse = 0;
    public $afstand2eKlasse = 0;
    public $indVolledig2eKlasse = false;
    public $prijsTraject = [];
}