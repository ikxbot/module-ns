<?php

namespace Ikx\NS\Model;

class Location
{
    public $name = '';
    public $lng = 0.00;
    public $lat = 0.00;
    public $city = '';
    public $countryCode = '';
    public $uicCode = '';
    public $weight = 0;
    public $products = 0;
}