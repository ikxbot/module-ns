<?php

namespace Ikx\NS\Model;

class TripFare
{
    public $priceInCents = 0;
    public $product = '';
    public $travelClass = '';
    public $priceInCentsExcludingSupplement = 0;
    public $discountType = '';
    public $supplementInCents = 0;
    public $link = '';
    public $priceInCentsExcludingSupplementOrPrice = 0;
}