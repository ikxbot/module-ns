<?php
namespace Ikx\NS\Model;

class Step {
    public $distanceInMeters = 0;
    public $durationInSeconds = 0;
    public $startLocation;
    public $endLocation;
    public $instructions = '';
}