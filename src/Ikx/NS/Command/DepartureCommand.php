<?php
namespace Ikx\NS\Command;

use Ikx\Core\Application;
use Ikx\Core\Command\AbstractCommand;
use Ikx\Core\Command\CommandInterface;
use Ikx\Core\Utils\Format;
use Ikx\Core\Utils\MessagingTrait;
use Ikx\NS\Lib\Exception\StationNotFoundException;
use Ikx\NS\Lib\NS\Api;

class DepartureCommand extends AbstractCommand implements CommandInterface
{
    use MessagingTrait;

    public $threaded = true;

    /**
     * Command runner
     */
    public function run()
    {
        if (!isset($this->params[0])) {
            $this->msg($this->channel, 'Please specify a station to fetch deparatures from.');
        } else {
            $apiKey = Application::config()->get('options.ns.key');
            if ($apiKey) {
                try {
                    $api = new Api($apiKey);
                    $departures = $api->departure(implode(' ', $this->params));
                    $this->msg($this->channel, Format::bold(sprintf('Departures for: %s', $departures['station'])));

                    $c = 0;

                    foreach($departures['data'] as $departure) {
                        $plannedDateTime = strtotime($departure->plannedDateTime);
                        $actualDateTime = strtotime($departure->actualDateTime);
                        $plannedTrack = $departure->plannedTrack;
                        $actualTrack = $departure->actualTrack ?? $departure->plannedTrack;
                        $trainCategory = $departure->product->shortCategoryName;
                        $direction = $departure->direction;
                        $messages = $departure->messages ?? [];
                        $routeStation = $departure->routeStations ?? [];
                        $cancelled = $departure->cancelled;

                        $trackString = $plannedTrack;
                        if ($plannedTrack != $actualTrack) {
                            $trackString = Format::color($actualTrack, 4);
                        }

                        $timeString = date('H:i', $plannedDateTime);
                        if ($plannedDateTime != $actualDateTime) {
                            $diffMin = ($actualDateTime - $plannedDateTime) / 60;
                            $timeString .= Format::color('+' . ceil($diffMin), 4);

                            // $timeString = Format::color(date('H:i', $actualDateTime), 4);
                        }

                        $additions = [];

                        if (count($routeStation)) {
                            $stations = [];
                            foreach($routeStation as $st) {
                                $stations[] = $st->mediumName;
                            }

                            $additions[] = 'via ' . implode(', ', $stations);
                        }

                        if (count($messages)) {
                            foreach($messages as $message) {
                                $additions[] = $message->message;
                            }
                        }

                        $append = 0;

                        if (count($additions)) {
                            $append = Format::color(' - ' . implode(' | ', $additions), 14);
                        }

                        $trainString = Format::bold(sprintf('%s %s to %s', $timeString, $trainCategory, $direction)) . ' ' . sprintf('on platform %s', $trackString);

                        if ($cancelled) {
                            $trainString = Format::color($trainString, 14);
                        }

                        $this->msg($this->channel, $trainString . $append);

                        $c++;

                        if ($c == 5) {
                            break;
                        }
                    }
                } catch(StationNotFoundException $e) {
                    $options = $api->searchStation(implode(' ', $this->params));
                    if (count($options)) {
                        $options = array_chunk($options, 10);
                        $this->msg($this->channel, 'Station not found. Did you mean one of these stations?');
                        $this->msg($this->channel, implode(', ', $options[0]));
                    } else {
                        $this->msg($this->channel, 'Station not found. Please try another station!');
                    }
                }
            } else {
                $this->msg($this->channel, 'NS module is not set up properly: please specify an API key in the configuration');
            }
        }
    }

    /**
     * Command describer (used for help)
     */
    public function describe()
    {
        return "Fetch NS departures for a given station";
    }
}