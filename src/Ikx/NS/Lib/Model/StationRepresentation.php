<?php
namespace Ikx\NS\Model;

class StationRepresentation {
    public $self = [];
    public $links = [];
    public $payload = [];
    public $meta = [];
}