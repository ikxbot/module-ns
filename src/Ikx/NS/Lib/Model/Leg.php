<?php
namespace Ikx\NS\Model;

class Leg {
    public $idx = '';
    public $name = '';
    public $travelType = '';
    public $direction = '';
    public $distance = 0;
    public $cancelled = false;
    public $changePossible = false;
    public $alternativeTransport = false;
    public $journeyStatus = '';
    public $journeyDetailRef = '';
    public $origin;
    public $destination;
    public $product;
    public $notes = [];
    public $messages = [];
    public $stops = [];
    public $steps = [];
    public $coordinates = [];
    public $crowdForecast = '';
    public $punctuality = 0.00;
    public $crossPlatformTransfer = false;
    public $shorterStock = false;
    public $changeCouldBePossible = false;
    public $journeyDetail = [];
    public $reachable = false;
    public $plannedDurationInMinutes = 0;
    public $travelAssistanceDeparture;
    public $travelAssistanceArrival;
    public $overviewPolyLine = [];
    public $firstStop;
    public $travelOrNeccesaryWalk = false;
    public $publicTransit = false;
    public $punctualityInternal = 0.00;
    public $lastStop;
}