<?php
namespace Ikx\NS\Model;

class TripOriginDestination {
    public $name = '';
    public $lat = 0.00;
    public $lng = 0.00;
    public $city = '';
    public $countryCode = '';
    public $uicCode = '';
    public $weight = 0;
    public $products = 0;
    public $type = '';
    public $prognosisType = '';
    public $plannedTimeZoneOffset = 0;
    public $plannedDateTime = '';
    public $actualTimeZoneOffset = 0;
    public $actualDateTime = '';
    public $plannedTrack = '';
    public $actualTrack = '';
    public $exitSide = '';
    public $checkinStatus = '';
    public $travelAssistanceBookingInfo;
    public $travelAssistanceMeetingPoints = [];
    public $notes = '';
    public $quayCode = '';
    public $domestic = false;
    public $latestKnownTrack = '';
}