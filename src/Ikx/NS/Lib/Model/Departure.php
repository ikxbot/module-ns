<?php
namespace Ikx\NS\Model;

class Departure {
    public $direction = '';
    public $name = '';
    public $plannedDateTime = '';
    public $actualDateTime = '';
    public $plannedTrack = '';
    public $actualTrack = '';
    public $product;
    public $trainCategory = '';
    public $cancelled = false;
    public $journeyDetailRef = '';
    public $messages = [];
    public $departureStatus = '';
}